#module: analizadorLexico.py
import lex,tablaPR, tablaSIM, tablaID
from time import gmtime, strftime


#Lista de tokens
tokens = (
    'ID',
    'NUM',
    'CAD',
    'LBRACE',
    'RBRACE',
    'SUM',
    'COMMA',
    'OBRACKET',
    'CBRACKET',
    'DOTCOMMA',
    'EQUALS',
    'PREI',
    'NEQ',
    'LAND',
    'PROMPT',
    'PRINT',
    'WHILE',
    'VAR',
    'FUNCTION',
    'RETURN',
    'INT',
    'BOOL',
    'STRING',
    'TRUE',
    'FALSE',
    'IF',
) 

#Creamos la tabla de ids globales
tablaID.tablaGeneral["global"] = dict()

#Literales para evaluacion al final (tabla interna)
literals = ['{','}','+','(',')',',',';','=']

# Regla para trackear numeros de linea
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# Caracteres a ignorar
def t_COMMENT(t):
    r'\/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+\/'
    pass
   
t_ignore  = ' \t'

# Gestion de errores
def t_error(t):
    with open('../files/output/logs.txt','a') as log:
        log.write('Error en ejecucion del dia' +strftime("%Y-%m-%d %H:%M:%S", gmtime())+' :\n')
        log.write("\t-Caracter ilegal '"+str(t.value[0])+ "' en linea " + str(t.lexer.lineno)+'\n')

    exit(1)


#Expresiones regulares y acciones semanticas para cada token
#Palabra reservada o ID
def t_ID(t):
    r'[a-zA-Z_]+[a-zA-Z_0-9]*'
    if t.value in tablaPR.palabrasReservadas:
        t.type = t.value.upper()
        t.value = tablaPR.palabrasReservadas.index(t.value)
    else:
        t.type = 'ID'
        #tablaID.tablaGeneral["global"][t.value] = dict()
        #tablaID.tablaGeneral["global"][t.value]["tipo"] = '-'

    return t

#Numeros
def t_NUM(t):
    r'\d+'
    if int(t.value).bit_length() > 16:
        with open('../files/output/logs.txt','a') as log:
            log.write('Error en ejecucion del dia' +strftime("%Y-%m-%d %H:%M:%S", gmtime())+' :\n')
            log.write('\t-Entero demasiado grande ('+ str(int(t.value).bit_length()) +' bits) en linea: '+ str(t.lexer.lineno)+'\n')
            exit(1)
    return t


#Preincremento
def t_PREI(t):
    r'\+\+'
    t.value = tablaSIM.simbolos.index(t.value)
    return t

#Igualdad negada
def t_NEQ(t):
    r'!='
    t.value = tablaSIM.simbolos.index(t.value)
    return t

#Y Logico
def t_LAND(t):
    r'&&'
    t.value = tablaSIM.simbolos.index(t.value)
    return t

#Cadena
def t_CAD(t):
    r'\'(.*?)\''
    t.value = t.value[1:]
    t.value = t.value[:-1]
    return t

#Corchete apertura
def t_LBRACE(t):
    r'\{'
    t.value = tablaSIM.simbolos.index(t.value)
    return t

#Corchete de cierre
def t_RBRACE(t):
    r'}'
    t.value = tablaSIM.simbolos.index(t.value)
    return t
#Suma
def t_SUM(t):
    r'\+'
    t.value = tablaSIM.simbolos.index(t.value)
    return t

#Coma
def t_COMMA(t):
    r'\,'
    t.value = tablaSIM.simbolos.index(t.value)
    return t


#Parentesis abierto
def t_OBRACKET(t):
    r'\('
    t.value = tablaSIM.simbolos.index(t.value)
    return t

#Parentesis cerrado
def t_CBRACKET(t):
    r'\)'
    t.value = tablaSIM.simbolos.index(t.value)
    return t

#Punto y coma
def t_DOTCOMMA(t):
    r'\;'
    t.value = tablaSIM.simbolos.index(t.value)
    return t

def t_EQUALS(t):
    r'\='
    t.value = tablaSIM.simbolos.index(t.value)
    return t




