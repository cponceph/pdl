#module: analizadorSintactico.py
import yacc,tablaID,tablaPR,scope
#Orden de ejecucion de reglas
parseorder = []

#Variables internas del sintactico
numeroparam = 0
scope = 0
scope_global = 1
paramstemp = dict()
#Lista de tokens
tokens = (
    'ID',
    'NUM',
    'CAD',
    'LBRACE',
    'RBRACE',
    'SUM',
    'COMMA',
    'OBRACKET',
    'CBRACKET',
    'DOTCOMMA',
    'EQUALS',
    'PREI',
    'NEQ',
    'LAND',
    'PROMPT',
    'PRINT',
    'WHILE',
    'VAR',
    'FUNCTION',
    'RETURN',
    'INT',
    'BOOL',
    'STRING',
    'TRUE',
    'FALSE',
    'IF',
) 

def p_program(p):
    '''program : funcion program regla1
               | compleja program regla2
               | lambda regla3'''
    

def p_funcion(p):
    'funcion : FUNCTION tipofuncion ID OBRACKET setscope paraf CBRACKET LBRACE compleja retorno DOTCOMMA RBRACE regla4'
    global numeroparam
    global paramstemp
    global scope_global
    numeroparam = 0
    #Generamos nueva tabla para la funcion
    tablaID.tablaGeneral[str(p[3]).lower()] = dict()
    #Anadimos tipo de retorno
    tablaID.tablaGeneral["global"][str(p[3]).lower()] = dict()
    tablaID.tablaGeneral["global"][str(p[3]).lower()]["tiporet"] = str(p[2])
    #Anadimos los parametros y sus tipos a la tabla
    for lexotipo, valor in paramstemp[scope][0].items():
        tablaID.tablaGeneral["global"][str(p[3]).lower()][lexotipo] = valor
    #Anadimos las declaraciones internas a la funcion a la tabla
    for ids, valor in paramstemp[scope][1].items():
        tablaID.tablaGeneral[str(p[3]).lower()][ids] = dict()
        tablaID.tablaGeneral[str(p[3]).lower()][ids]["tipo"] = valor
    #Salimos del scope de la funcion
    scope_global = 1

    

def p_compleja(p):
    '''compleja : WHILE OBRACKET derechol CBRACKET LBRACE compleja RBRACE regla5
                | VAR tipo ID DOTCOMMA regla6
                | simple DOTCOMMA regla7
                | IF OBRACKET derechol CBRACKET sentenciaif DOTCOMMA regla8
                | io DOTCOMMA regla9
                | lambda regla10'''
    if(isinstance(p[1],int) and tablaPR.palabrasReservadas[p[1]] == "var"):
        if(scope_global == 1):
            tablaID.tablaGeneral["global"][p[3].lower()] = dict()
            tablaID.tablaGeneral["global"][p[3].lower()]["tipo"] = p[2]
        else:
            paramstemp[scope][1][p[3]] = p[2]
        
    

def p_sentenciaif(p):
    '''sentenciaif : simple regla11
                   | io regla12
                   | retorno regla13'''
    

def p_io(p):
    '''io : PROMPT OBRACKET ID CBRACKET regla14
          | PRINT OBRACKET expresion CBRACKET regla15'''
    

def p_retorno(p):
    'retorno : RETURN tiporet regla16'
    

def p_simple(p):
    '''simple : ID derecho regla17
              | CAD regla18'''
    

def p_tipo(p):
    '''tipo : INT regla19
            | BOOL regla20
            | STRING regla21'''
    p[0] = tablaPR.palabrasReservadas[p[1]]
    
    

def p_tipofuncion(p):
    '''tipofuncion : tipo regla22
                   | lambda regla23'''
    p[0] = p[1]
    

def p_tiporet(p):
    '''tiporet : expresion regla24
               | io regla25
               | CAD regla26
               | lambda regla27'''
    
def p_derecho(p):
    '''derecho : OBRACKET llamada CBRACKET regla28
               | EQUALS expresion regla29'''
       
def p_llamada(p):
    '''llamada : expresion llamada1 regla30
               | CAD llamada1 regla31
               | lambda regla32'''
    
def p_llamada1(p):
    '''llamada1 : COMMA llamada2 regla33
                | lambda regla34'''
    
def p_llamada2(p):
    '''llamada2 : expresion llamada1 regla35
                | CAD llamada1 regla36'''
    
def p_paraf(p):
    '''paraf : tipo ID paraf2 regla37
             | lambda regla38'''
    if(len(p) == 5):
        global numeroparam
        global paramstemp
        numeroparam = numeroparam +1
        paramstemp[scope][0]["tipo"+str(numeroparam)] = p[1]
        paramstemp[scope][0]["parametro"+str(numeroparam)] = p[2].lower()
        
            
def p_paraf2(p):
    '''paraf2 : COMMA paraf3 regla39
              | lambda regla40'''
    
def p_paraf3(p):
    'paraf3 : tipo ID paraf2 regla41'
    if(len(p) == 5):
        global numeroparam
        global paramstemp
        numeroparam = numeroparam +1
        paramstemp[scope][0]["tipo"+str(numeroparam)] = p[1]
        paramstemp[scope][0]["parametro"+str(numeroparam)] = p[2].lower()
        
    
def p_expresion(p):
    '''expresion : PREI ID x opi regla42
                 | ID x opi regla43
                 | OBRACKET expresion CBRACKET opi regla44
                 | logvalue opl regla45
                 | NUM op regla46'''
    
def p_derechol(p):
    '''derechol : ID x opl regla47
                | logvalue opl regla48
                | OBRACKET derechol CBRACKET opl regla49'''
    
def p_derechon(p):
    '''derechon : PREI ID x op regla50
                | ID x op regla51
                | NUM op regla52
                | OBRACKET derechon CBRACKET op regla53'''
    
def p_x(p):
    '''x : OBRACKET llamada CBRACKET regla54
         | lambda regla55'''
    
def p_opi(p):
    '''opi : NEQ expresion regla56
           | LAND expresion regla57
           | SUM expresion regla58
           | lambda regla59'''
    
def p_opl(p):
    '''opl : NEQ derechol regla60
           | LAND derechol regla61
           | lambda regla62'''
    
def p_op(p):
    '''op : SUM derechon regla63
          | NEQ derechon regla64
          | lambda regla65'''
    
def p_logvalue(p):
    '''logvalue : FALSE regla66
                | TRUE regla67'''
    
def p_lambda(p):
     'lambda :'
     p[0] = "ninguno"
     
 # Error rule for syntax errors
def p_error(p):
    print("Syntax error in input!")

#Numeros de regla



def p_regla1(p):
	"regla1 :"
	parseorder.append(1)
def p_regla2(p):
	"regla2 :"
	parseorder.append(2)
def p_regla3(p):
	"regla3 :"
	parseorder.append(3)
def p_regla4(p):
	"regla4 :"
	parseorder.append(4)
def p_regla5(p):
	"regla5 :"
	parseorder.append(5)
def p_regla6(p):
	"regla6 :"
	parseorder.append(6)
def p_regla7(p):
	"regla7 :"
	parseorder.append(7)
def p_regla8(p):
	"regla8 :"
	parseorder.append(8)
def p_regla9(p):
	"regla9 :"
	parseorder.append(9)
def p_regla10(p):
	"regla10 :"
	parseorder.append(10)
def p_regla11(p):
	"regla11 :"
	parseorder.append(11)
def p_regla12(p):
	"regla12 :"
	parseorder.append(12)
def p_regla13(p):
	"regla13 :"
	parseorder.append(13)
def p_regla14(p):
	"regla14 :"
	parseorder.append(14)
def p_regla15(p):
	"regla15 :"
	parseorder.append(15)
def p_regla16(p):
	"regla16 :"
	parseorder.append(16)
def p_regla17(p):
	"regla17 :"
	parseorder.append(17)
def p_regla18(p):
	"regla18 :"
	parseorder.append(18)
def p_regla19(p):
	"regla19 :"
	parseorder.append(19)
def p_regla20(p):
	"regla20 :"
	parseorder.append(20)
def p_regla21(p):
	"regla21 :"
	parseorder.append(21)
def p_regla22(p):
	"regla22 :"
	parseorder.append(22)
def p_regla23(p):
	"regla23 :"
	parseorder.append(23)
def p_regla24(p):
	"regla24 :"
	parseorder.append(24)
def p_regla25(p):
	"regla25 :"
	parseorder.append(25)
def p_regla26(p):
	"regla26 :"
	parseorder.append(26)
def p_regla27(p):
	"regla27 :"
	parseorder.append(27)
def p_regla28(p):
	"regla28 :"
	parseorder.append(28)
def p_regla29(p):
	"regla29 :"
	parseorder.append(29)
def p_regla30(p):
	"regla30 :"
	parseorder.append(30)
def p_regla31(p):
	"regla31 :"
	parseorder.append(31)
def p_regla32(p):
	"regla32 :"
	parseorder.append(32)
def p_regla33(p):
	"regla33 :"
	parseorder.append(33)
def p_regla34(p):
	"regla34 :"
	parseorder.append(34)
def p_regla35(p):
	"regla35 :"
	parseorder.append(35)
def p_regla36(p):
	"regla36 :"
	parseorder.append(36)
def p_regla37(p):
	"regla37 :"
	parseorder.append(37)
def p_regla38(p):
	"regla38 :"
	parseorder.append(38)
def p_regla39(p):
	"regla39 :"
	parseorder.append(39)
def p_regla40(p):
	"regla40 :"
	parseorder.append(40)
def p_regla41(p):
	"regla41 :"
	parseorder.append(41)
def p_regla42(p):
	"regla42 :"
	parseorder.append(42)
def p_regla43(p):
	"regla43 :"
	parseorder.append(43)
def p_regla44(p):
	"regla44 :"
	parseorder.append(44)
def p_regla45(p):
	"regla45 :"
	parseorder.append(45)
def p_regla46(p):
	"regla46 :"
	parseorder.append(46)
def p_regla47(p):
	"regla47 :"
	parseorder.append(47)
def p_regla48(p):
	"regla48 :"
	parseorder.append(48)
def p_regla49(p):
	"regla49 :"
	parseorder.append(49)
def p_regla50(p):
	"regla50 :"
	parseorder.append(50)
def p_regla51(p):
	"regla51 :"
	parseorder.append(51)
def p_regla52(p):
	"regla52 :"
	parseorder.append(52)
def p_regla53(p):
	"regla53 :"
	parseorder.append(53)
def p_regla54(p):
	"regla54 :"
	parseorder.append(54)
def p_regla55(p):
	"regla55 :"
	parseorder.append(55)
def p_regla56(p):
	"regla56 :"
	parseorder.append(56)
def p_regla57(p):
	"regla57 :"
	parseorder.append(57)
def p_regla58(p):
	"regla58 :"
	parseorder.append(58)
def p_regla59(p):
	"regla59 :"
	parseorder.append(59)
def p_regla60(p):
	"regla60 :"
	parseorder.append(60)
def p_regla61(p):
	"regla61 :"
	parseorder.append(61)
def p_regla62(p):
	"regla62 :"
	parseorder.append(62)
def p_regla63(p):
	"regla63 :"
	parseorder.append(63)
def p_regla64(p):
	"regla64 :"
	parseorder.append(64)
def p_regla65(p):
	"regla65 :"
	parseorder.append(65)
def p_regla66(p):
	"regla66 :"
	parseorder.append(66)
def p_regla67(p):
	"regla67 :"
	parseorder.append(67)

#Crear una lista de scope temporal y escribir al final
def p_setscope(p):
    "setscope :"
    global scope
    global paramstemp
    global scope_global
    scope_global = 0
    scope = scope + 1
    paramstemp[scope] = [dict(),dict()]


     
#Gestion de tablas











