import lex,yacc,analizadorLexico,analizadorSintactico,tablaID


#Definicion de estructuras de datos globales
tokens = []


#Abrimos el fichero de codigo fuente
with open('../files/input/Correctas/Sintactico/prueba3.txt','r') as archivo:
    data = archivo.read()
##Analizador lexico
lexer = lex.lex(module=analizadorLexico)
lexer.input(data)
#Tokenize
while True:
    tok = lexer.token()
    if not tok:
        break
    tokens.append(tok)
    print(tok)

##Analizador sintactico
parser = yacc.yacc(module=analizadorSintactico)
#Generamos el resultado del parse
p = parser.parse(input=data,lexer=lexer,debug=False)
parseorder = analizadorSintactico.parseorder

##Salidas del programa
#Generacion del fichero de tokens
with open('../files/output/tokens.txt','w') as tokenfile:
    tokenfile.truncate()
    for token in tokens:
        tokenfile.write("< " + str(token.type) + " , " + str(token.value) + " >\n")

#Generacion del fichero de resultado del parse
with open('../files/output/parse.txt','w') as parsefile:
    parsefile.truncate()
    parsefile.write("Ascendente ")
    for x in range(len(parseorder)): 
        parsefile.write(str(parseorder[x]))
        parsefile.write(" ")



#Generacion del fichero de tabla de simbolos
with open('../files/output/tablas.txt','w') as tablasfile:
    tablasfile.truncate()
#Imprimimos la tabla de palabras reservadas
    contador_tablas = 1
    for nombre,tabla in tablaID.tablaGeneral.items():
        tablasfile.write("Tabla de identificadores " +str(nombre)+" #"+ str(contador_tablas)+":\n" )
        contador_tablas = contador_tablas+1
        for lex,atrib in tabla.items():
            tablasfile.write("* LEXEMA : " + "\'"+str(lex)+"\'"+ "\n")
            for nombre,valor in atrib.items():
                tablasfile.write("  + "+str(nombre)+" : "+str(valor)+"\n")
    



